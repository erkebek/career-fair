package io.shaurma.careerfair;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.shaurma.careerfair.fragments.EventFragment;
import io.shaurma.careerfair.fragments.ExhibitorsFragment;
import io.shaurma.careerfair.fragments.HomeFragment;
import io.shaurma.careerfair.fragments.LectureFragment;
import io.shaurma.careerfair.fragments.ListFragment;
import io.shaurma.careerfair.helpers.CacheHelper;
import io.shaurma.careerfair.helpers.JsonHelper;
import io.shaurma.careerfair.helpers.VolleyCallback;
import io.shaurma.careerfair.models.Event;
import io.shaurma.careerfair.models.Exhibitor;
import io.shaurma.careerfair.models.FairItem;
import io.shaurma.careerfair.models.Lecture;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "mainActivityTag";

    static List<FairItem> userList = null;

    private ArrayAdapter<FairItem> userListAdapter;

    private final String UL_LECTURES_FILE_NAME = "user_lec_list";
    private final String UL_EVENTS_FILE_NAME = "user_ev_list";
    private final String UL_EXHIBITORS_FILE_NAME = "user_ex_list";

    public static MainActivity instance;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        Fragment selectedFragment = null;
        switch (item.getItemId()) {
            case R.id.nav_home:
                sortUserList();
                selectedFragment = HomeFragment.getInstance();
                break;
            case R.id.nav_list:
                selectedFragment = ListFragment.getInstance();
                break;
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, selectedFragment);
        transaction.commit();
        return true;
    };

    private BottomNavigationView.OnNavigationItemReselectedListener mOnNavigationItemReselectedListener
            = item -> {
        Fragment selectedFragment = null;
        switch (item.getItemId()) {
            case R.id.nav_home:
                break;
            case R.id.nav_list:
                break;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setOnNavigationItemReselectedListener(mOnNavigationItemReselectedListener);

        loadData();


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, HomeFragment.getInstance());
        transaction.commit();
    }

    public void setActionBarTitle(String title) {
        if (title != null) {
            if (!getSupportActionBar().isShowing())
                getSupportActionBar().show();

            getSupportActionBar().setTitle(title);
        } else {
            getSupportActionBar().hide();
        }
    }

    public void makeGetRequest(String url, final VolleyCallback callback) {
        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> callback.onSuccess(response),
                error -> System.out.println("Error"));
        queue.add(stringRequest);
    }

    public void loadData() {
        loadUserList();
    }

    private void loadUserList() {
        String exJson = CacheHelper.retrieve(this, UL_EXHIBITORS_FILE_NAME);
        userList = new ArrayList<>();
        if (exJson.length() > 0) {
            List<Exhibitor> exList = new JsonHelper<Exhibitor>().getArrayFromJson(exJson, Exhibitor.class);
            userList.addAll(exList);
            Log.d(TAG, String.valueOf(exList.size()));
        }
        String lecJson = CacheHelper.retrieve(this, UL_LECTURES_FILE_NAME);
        if (lecJson.length() > 0) {
            List<Lecture> lecList = new JsonHelper<Lecture>().getArrayFromJson(lecJson, Lecture.class);
            userList.addAll(lecList);
        }
        String evJson = CacheHelper.retrieve(this, UL_EVENTS_FILE_NAME);
        if (evJson.length() > 0) {
            List<Event> evList = new JsonHelper<Event>().getArrayFromJson(evJson, Event.class);
            userList.addAll(evList);
        }
        sortUserList();
    }

    public static List<FairItem> getUserList() {
        return userList;
    }

    public static void sortUserList() {
        Collections.sort(userList);
    }

    private void writeUserListToCache() {
        List<Exhibitor> exList = new ArrayList<>();
        List<Lecture> lecList = new ArrayList<>();
        List<Event> evList = new ArrayList<>();

        for (FairItem item : userList) {
            if (item instanceof Exhibitor) exList.add((Exhibitor) item);
            if (item instanceof Lecture) lecList.add((Lecture) item);
            if (item instanceof Event) evList.add((Event) item);
        }

        String exJson = new JsonHelper<Exhibitor>().getStringFromJson(exList, Exhibitor.class);
        String lecJson = new JsonHelper<Lecture>().getStringFromJson(lecList, Lecture.class);
        String evJson = new JsonHelper<Event>().getStringFromJson(evList, Event.class);

        CacheHelper.save(this, UL_EXHIBITORS_FILE_NAME, exJson);
        CacheHelper.save(this, UL_LECTURES_FILE_NAME, lecJson);
        CacheHelper.save(this, UL_EVENTS_FILE_NAME, evJson);
    }

    @Override
    public void onStop() {
        super.onStop();
        writeUserListToCache();
    }

    public static FairItem getFromUserList(FairItem target) {
        if (userList.contains(target)) {
            for (FairItem item : userList) {
                if (target.equals(item)) {
                    return item;
                }
            }
        }
        return null;
    }
}
