package io.shaurma.careerfair.adapters;


import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import io.shaurma.careerfair.DetailActivity;
import io.shaurma.careerfair.MainActivity;
import io.shaurma.careerfair.R;
import io.shaurma.careerfair.models.Exhibitor;
import io.shaurma.careerfair.fragments.AddDialogFragment;

public class ExhibitorsRVAdapter extends RecyclerView.Adapter<ExhibitorsRVAdapter.ExhibitorViewHolder> {

    private String TAG = "adapterTag";

    public static class ExhibitorViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView companyName;
        ImageView companyLogo;
        TextView companyTime;
        public FloatingActionButton floatingActBtn;

        ExhibitorViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cv_arbeitgeber);
            companyName = (TextView) itemView.findViewById(R.id.home_name);
            companyLogo = (ImageView) itemView.findViewById(R.id.arbeitgeber_logo);
            companyTime = (TextView) itemView.findViewById(R.id.arbeitgeber_time);
            floatingActBtn = (FloatingActionButton) itemView.findViewById(R.id.home_fab);
        }

    }


    List<Exhibitor> exhibitors;
    Activity mainActivity;


    public ExhibitorsRVAdapter(List<Exhibitor> exhibitor, Activity mainActivity) {
        this.exhibitors = exhibitor;
        this.mainActivity = mainActivity;
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public ExhibitorViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.exhibitor_item, viewGroup, false);
        ExhibitorViewHolder pvh = new ExhibitorViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(ExhibitorViewHolder exhibitorViewHolder, int i) {
        exhibitorViewHolder.cv.setOnClickListener(v -> {
            Intent intent = new Intent(getMainActivity(), DetailActivity.class);
            intent.putExtra(DetailActivity.fairItemToSendId, i);
            intent.putExtra(DetailActivity.fairItemToSendType, DetailActivity.fairItemToSendTypeExhibitor);
            getMainActivity().startActivity(intent);
        });
        exhibitorViewHolder.companyName.setText(exhibitors.get(i).getName());
        Picasso.get().load(exhibitors.get(i).getLogo())
                .resize(100, 100)
                .centerInside().into(exhibitorViewHolder.companyLogo);
        exhibitorViewHolder.companyTime.setText(exhibitors.get(i).getDuration());

        if (MainActivity.getUserList().contains(exhibitors.get(i))) {
            loadRemoveButton(exhibitorViewHolder, i);
        } else {
            loadAddButton(exhibitorViewHolder, i);
        }
    }

    private void loadAddButton(ExhibitorViewHolder viewHolder, int position) {
        viewHolder.floatingActBtn.setBackgroundTintList(viewHolder.itemView.getResources().getColorStateList(android.R.color.holo_green_light));
        viewHolder.floatingActBtn.setImageResource(R.drawable.ic_sharp_add_white_24px);
        viewHolder.floatingActBtn.setOnClickListener(v -> {
            Log.d(TAG, "Add Button clicked. id: " + exhibitors.get(position).getName());
            FragmentManager manager = getMainActivity().getFragmentManager();
            AddDialogFragment dialogFragment = new AddDialogFragment();
            dialogFragment.setAdapter(this);
            dialogFragment.setViewHolder(viewHolder);
            dialogFragment.setSelectedItem(exhibitors.get(position));
            dialogFragment.show(manager, "dialog");
        });
    }

    private void loadRemoveButton(ExhibitorViewHolder viewHolder, int position) {
        viewHolder.floatingActBtn.setBackgroundTintList(viewHolder.itemView.getResources().getColorStateList(android.R.color.holo_red_light));
        viewHolder.floatingActBtn.setImageResource(R.drawable.ic_pencil_edit_button_white_24);
        viewHolder.floatingActBtn.setOnClickListener(v -> {
            Log.d(TAG, "Edit Button clicked. id: " + exhibitors.get(position).getName());
            FragmentManager manager = getMainActivity().getFragmentManager();
            AddDialogFragment dialogFragment = new AddDialogFragment();
            dialogFragment.setViewHolder(viewHolder);
            dialogFragment.setAdapter(this);
            dialogFragment.setSelectedItem(MainActivity.getFromUserList(exhibitors.get(position)));
            dialogFragment.setEditDialog(true);
            dialogFragment.show(manager, "dialog");
        });
    }

    @Override
    public int getItemCount() {
        return exhibitors.size();
    }

    public Activity getMainActivity() {
        return mainActivity;
    }
}
