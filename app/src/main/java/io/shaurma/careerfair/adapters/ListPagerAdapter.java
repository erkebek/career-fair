package io.shaurma.careerfair.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import io.shaurma.careerfair.MainActivity;
import io.shaurma.careerfair.R;
import io.shaurma.careerfair.fragments.ExhibitorsFragment;
import io.shaurma.careerfair.fragments.EventFragment;
import io.shaurma.careerfair.fragments.LectureFragment;

public class ListPagerAdapter extends FragmentPagerAdapter {

    private MainActivity mainActivity;

    public ListPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ExhibitorsFragment();
            case 1:
                return new LectureFragment();
            case 2:
                return new EventFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return getMainActivity().getResources().getString(R.string.tab_title_employer);
            case 1:
                return getMainActivity().getResources().getString(R.string.tab_title_presentation);
            case 2:
                return getMainActivity().getResources().getString(R.string.tab_title_events);
            default:
                return null;
        }
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }
}
