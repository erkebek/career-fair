package io.shaurma.careerfair.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.List;

import io.shaurma.careerfair.R;
import io.shaurma.careerfair.adapters.ExhibitorsRVAdapter;
import io.shaurma.careerfair.helpers.CacheHelper;
import io.shaurma.careerfair.helpers.JsonHelper;
import io.shaurma.careerfair.helpers.VolleyCallback;
import io.shaurma.careerfair.models.Exhibitor;
import io.shaurma.careerfair.models.FairItem;

public class ExhibitorsFragment extends Fragment {

    RecyclerView rv;
    SwipeRefreshLayout mSwipeRefreshLayout;

    private String TAG = "ARBEITGEBER_FRAGMENT";

    static List<Exhibitor> exhibitors = new ArrayList<>();

    boolean isInit = false;

    private final String EXHIBITORS_URL = "http://lab.wikway.de/companies/zwik-exhibitors-json";
    private final String EXHIBITORS_FILE_NAME = "exhibitors";


    public ExhibitorsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_exhibitor, container, false);

        rv = (RecyclerView) view.findViewById(R.id.rv_arbeitgeber);

        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container_exhibitor);
        mSwipeRefreshLayout.setOnRefreshListener(() -> parseExhibitors());
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        //loadExhibitors();
        new MyTask().execute();

        ExhibitorsRVAdapter adapter = new ExhibitorsRVAdapter(exhibitors, getActivity());
        rv.setAdapter(adapter);

        return view;
    }

    private Fragment loadExhibitors() {
        String json = CacheHelper.retrieve(getActivity(), EXHIBITORS_FILE_NAME);
        if (json.length() > 0) {
            exhibitors = new JsonHelper<Exhibitor>().getArrayFromJson(json, Exhibitor.class);
            isInit = true;
        } else {
            parseExhibitors();
        }
        if (isInit) {
            for (FairItem item : exhibitors) {
                item.loadDates();
            }
            mSwipeRefreshLayout.setRefreshing(false);

            return this;
        }

        return this;
    }

    public void parseExhibitors() {
        mSwipeRefreshLayout.setRefreshing(true);
        VolleyCallback exhibitorCallback = (json) -> {
            if (this.isVisible()) {
                exhibitors = new JsonHelper<Exhibitor>().getArrayFromJson(json, Exhibitor.class);
                isInit = true;
                CacheHelper.save(getActivity(), EXHIBITORS_FILE_NAME, json);
                loadExhibitors();
                if(this.isVisible()) {
                    ExhibitorsRVAdapter adapter = new ExhibitorsRVAdapter(exhibitors, getActivity());
                    rv.setAdapter(adapter);
                }
            }
        };
        makeGetRequest(EXHIBITORS_URL, exhibitorCallback);
    }

    public void makeGetRequest(String url, final VolleyCallback callback) {
        RequestQueue queue = Volley.newRequestQueue(getActivity());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> callback.onSuccess(response),
                error -> System.out.println("Error"));
        queue.add(stringRequest);
    }

    public static List<Exhibitor> getExhibitors() {
        return exhibitors;
    }

    class MyTask extends AsyncTask<Void, Void, Fragment> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mSwipeRefreshLayout.setRefreshing(true);
        }

        @Override
        protected Fragment doInBackground(Void... params) {

            return loadExhibitors();
        }

        @Override
        protected void onPostExecute(Fragment result) {
            super.onPostExecute(result);

            if(result.isVisible()) {
                ExhibitorsRVAdapter adapter = new ExhibitorsRVAdapter(exhibitors, getActivity());
                rv.setAdapter(adapter);
            }
        }
    }
}