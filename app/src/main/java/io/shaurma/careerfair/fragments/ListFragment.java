package io.shaurma.careerfair.fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import io.shaurma.careerfair.adapters.ListPagerAdapter;
import io.shaurma.careerfair.MainActivity;
import io.shaurma.careerfair.R;


public class ListFragment extends Fragment {

    public MainActivity mainActivity;
    private static ListFragment instance;

    public static ListFragment getInstance() {
        if (instance == null) {
            instance = new ListFragment();
        }
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((MainActivity) getActivity()).setActionBarTitle("List");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list, container, false);

        ViewPager viewPager = (ViewPager) view.findViewById(R.id.pager);
        ListPagerAdapter listPagerAdapter = new ListPagerAdapter(getChildFragmentManager());
        listPagerAdapter.setMainActivity((MainActivity) getActivity());
        viewPager.setAdapter(listPagerAdapter);
        //setupViewPager(viewPager);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getChildFragmentManager());
        adapter.addFragment(new ExhibitorsFragment(), getMainActivity().getResources().getString(R.string.tab_title_employer));
        adapter.addFragment(new LectureFragment(), getMainActivity().getResources().getString(R.string.tab_title_presentation));
        adapter.addFragment(new EventFragment(), getMainActivity().getResources().getString(R.string.tab_title_events));
        viewPager.setAdapter(adapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }

}
