package io.shaurma.careerfair.helpers;

public interface VolleyCallback {
    void onSuccess(String json);
}
