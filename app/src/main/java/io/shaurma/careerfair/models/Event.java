package io.shaurma.careerfair.models;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.util.Objects;

public class Event extends  FairItem {
    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    public Event(String name) throws ParseException {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Event{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return (super.getBegin() != "")?
                Objects.equals(name, event.name) &&
                        Objects.equals(description, event.description) &&
                        Objects.equals(super.getBegin(), event.getBegin()) :
                Objects.equals(name, event.name) &&
                        Objects.equals(description, event.description);

    }

    @Override
    public int hashCode() {

        return Objects.hash(name, description);
    }
}
