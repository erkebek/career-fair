package io.shaurma.careerfair.models;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.util.Objects;

public class Exhibitor extends FairItem {
    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("link")
    private String link;

    @SerializedName("logo")
    private String logo;

    public Exhibitor(String name, String description, String link, String logo) throws ParseException {
        super();
        this.name = name;
        this.description = description;
        this.link = link;
        this.logo = logo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    @Override
    public String toString() {
        return "Exhibitor{" +
                "name='" + name + '\'' +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exhibitor exhibitor = (Exhibitor) o;
        return Objects.equals(name, exhibitor.name) &&
                Objects.equals(link, exhibitor.link);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, link, logo);
    }
}
