package io.shaurma.careerfair.models;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public abstract class FairItem implements Comparable<FairItem> {

    @SerializedName("time_required_to_visit")
    private String timeRequiredToVisit;

    @SerializedName("begin")
    private String begin;
    private Date beginDate;

    @SerializedName("end")
    private String end;
    private Date endDate;
    private int duration;
    private boolean important;

    public FairItem() {

    }

    public void loadDates() {
        try {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            calendar.setTime(format.parse(getTimeRequiredToVisit()));
            duration = calendar.get(Calendar.MINUTE) + calendar.get(Calendar.HOUR_OF_DAY) * 60;

            calendar.setTime(format.parse(begin));
            beginDate = calendar.getTime();
            calendar.add(Calendar.MINUTE, duration);
            endDate = calendar.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public String getTimeRequiredToVisit() {
        return timeRequiredToVisit;
    }
    public String getDuration() {
        if (duration != 0) {
            return duration + " min";
        }
        return "Not specified";
    }

    public void setTimeRequiredToVisit(String timeRequiredToVisit) {
        this.timeRequiredToVisit = timeRequiredToVisit;
    }

    public void planToTime(Date beginDate, Date endDate, int duration) {
        this.duration = duration;
        this.beginDate = beginDate;
        this.endDate = endDate;
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        setBegin(format.format(beginDate));
        setEnd(format.format(endDate));
    }

    public void setTime(Date beginDate, Date endDate, int duration) {
        if (!hasTimeSpecified()) {
            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            this.duration = duration;
            this.beginDate = beginDate;
            this.endDate = endDate;
            setBegin(format.format(beginDate));
            setEnd(format.format(endDate));
        }
    }

    public String getBegin() {
        return begin == null ? "" : begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getEnd() {
        return end == null ? "" : end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public boolean hasTimeSpecified() {
        return getBegin() != null && getBegin().length() > 0;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    abstract public boolean equals(Object obj);

    @Override
    public int compareTo(FairItem item) {
        return getBeginDate().compareTo(item.getBeginDate());
    }

    public boolean isImportant() {
        return important;
    }

    public void setImportant(boolean important) {
        this.important = important;
    }
}
