package io.shaurma.careerfair.models;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.util.Objects;

public class Lecture extends FairItem {
    @SerializedName("company_name")
    private String companyName;

    @SerializedName("lecture_title")
    private String lectureTitle;

    public Lecture(String companyName, String lectureTitle) throws ParseException {
        super();
        this.companyName = companyName;
        this.lectureTitle = lectureTitle;
    }

    public String getLectureTitle() {
        return lectureTitle;
    }

    public void setLectureTitle(String lectureTitle) {
        this.lectureTitle = lectureTitle;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public String toString() {
        return "Lecture{" +
                "companyName='" + companyName + '\'' +
                ", lectureTitle='" + lectureTitle + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lecture lecture = (Lecture) o;
        return Objects.equals(companyName, lecture.companyName) &&
                Objects.equals(lectureTitle, lecture.lectureTitle);
    }

    @Override
    public int hashCode() {

        return Objects.hash(companyName, lectureTitle);
    }
}
